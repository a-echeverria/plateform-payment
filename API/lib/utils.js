exports.prettifyValidationErrors = (errors) =>
  Object.keys(errors).reduce((acc, err) => {
    acc[err] = errors[err].message;
    return acc;
  }, {});

exports.OperationStatus = ["EN ATTENTE", "CONFIRMER", "VALIDATED", "ANNULER", "REFUSER", "REFUND"];

exports.generateClientIdAndSecret = (length = 20) => {
  function generateChars(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  return {
    client_id: generateChars(length),
    client_secret: generateChars(length),
  }
};
