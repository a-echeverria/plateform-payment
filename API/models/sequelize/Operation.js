const {Model, DataTypes} = require("sequelize");
const conn = require("../../lib/sequelize");
const {OperationStatus} = require('../../lib/utils');

class Operation extends Model {}

Operation.init(
  {
    status: DataTypes.STRING,
    price: DataTypes.INTEGER,
    item: DataTypes.ARRAY(DataTypes.STRING),
    currency: {
      type: DataTypes.STRING,
      defaultValue: "EURO",
      allowNull: false,
    },
  },
  {
    timestamps: true,
    sequelize: conn,
    modelName: "Operation",
    freezeTableName: true,
  }
);

module.exports = Operation;
