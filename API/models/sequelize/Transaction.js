const {Model, DataTypes} = require("sequelize");
const Operation = require("./Operation");
const conn = require("../../lib/sequelize");

class Transaction extends Model {}

Transaction.init(
  {
    clientFirstName: DataTypes.STRING,
    clientLastName: DataTypes.STRING,
    clientAddress: DataTypes.STRING,
    clientEmail: DataTypes.STRING,
  },
  {
    timestamps: true,
    sequelize: conn,
    modelName: "Transaction",
    freezeTableName: true,
  }
);

// One To Many
Operation.belongsTo(Transaction, {as: "transaction"});
Transaction.hasMany(Operation, {foreignKey: "transactionId"});

module.exports = Transaction;
