const {Model, DataTypes} = require("sequelize");
const Transaction = require("./Transaction");
const conn = require("../../lib/sequelize");
const bcrypt = require("bcryptjs");

class Marchand extends Model {}

Marchand.init(
  {
    businessName: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true,
      },
      unique: true,
    },
    phoneNumber: DataTypes.STRING,
    currency: DataTypes.STRING,
    confirmationUrl: DataTypes.STRING,
    cancelUrl: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    confirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    client_id: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    },
    client_secret: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    },
      is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    }
  },
  {
    timestamps: true,
    sequelize: conn,
    modelName: "Marchand",
    freezeTableName: true,
  }
);

const updatePassword = async (marchand) => {
  marchand.password = await bcrypt.hash(marchand.password, await bcrypt.genSalt());
};

Marchand.addHook("beforeCreate", updatePassword);
// Marchand.addHook("beforeUpdate", updatePassword);

// One To Many
Transaction.belongsTo(Marchand, {as: "marchand"});
Marchand.hasMany(Transaction, {foreignKey: "marchandId"});

module.exports = Marchand;
