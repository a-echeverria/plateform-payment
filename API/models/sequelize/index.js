const connection = require("../../lib/sequelize");
const Operation = require('./Operation');
const Transaction = require('./Transaction');
const Marchand = require("./Marchand");
const {TransactionOperation, OperationHistory} = require("../mongoose");

connection.sync({alter: true});

const denormalizeTransaction = async (transaction) => {
  transaction = {...transaction.dataValues}
  transaction = await Transaction.findByPk(transaction.id, {
    include: [Operation],
  });

  const marchand = await Marchand.findByPk(transaction.marchandId);
  transaction = {...transaction.dataValues}

  const transactionOperation = {
    transactionId: transaction.id,
    price: transaction.Operations.reduce((acc, cur) => acc + cur.price, 0),
    currency: marchand.currency,
    client: {
      firstName: transaction.clientFirstName,
      lastName: transaction.clientLastName,
      email: transaction.clientEmail,
      address: transaction.clientAddress,
    },
    marchand: {
      id: marchand.id,
      businessName: marchand.businessName,
      email: marchand.email,
      phoneNumber: marchand.phoneNumber,
    },
    operations: transaction.Operations,
  };
  await TransactionOperation.findOneAndReplace(
    {
      transactionId: transaction.id
    },
    transactionOperation,
    {
      upsert: true,
      new: true
    }).catch(e => console.error(e));
};

const createOperationHistory = async (operation) => {
  const operationHistory = {...operation, operationId: operation.id};
  delete operationHistory.id;
  delete operationHistory.updatedAt;
  delete operationHistory.createdAt;
  await OperationHistory.create(operationHistory).catch(e => console.error(e));
};

const updateTransactionOperation = async (operation) => {
  const transaction = await TransactionOperation.findOne({transactionId: operation.transactionId});
  if (!transaction) {
    return;
  }
  const index = transaction.operations.findIndex(t => t.id === operation.id);
  if (index === -1) {
    // New operation
    await TransactionOperation.updateOne({
      transactionId: operation.transactionId
    }, {
      price: transaction.price + operation.price,
      $push: {operations: operation},
      currency: operation.currency,
    }).catch(e => console.error('Error on adding operation to TransactionOperation :', e));
  } else {
    // Update already existing operation
    await TransactionOperation.updateOne({
      transactionId: operation.transactionId,
    }, {
      $set: {
        ["operations." + index]: operation,
      }
    })
      .catch(e => console.error('Error on updating operation to TransactionOperation :', e));
  }
};

Operation.addHook('afterCreate', async (operation) => {
  operation = {...operation.dataValues}
  await createOperationHistory(operation);
  await updateTransactionOperation(operation);
});

Operation.addHook('afterUpdate', async (operation) => {
  operation = {...operation.dataValues}
  await createOperationHistory(operation);
  await updateTransactionOperation(operation);
})

Transaction.addHook("afterCreate", denormalizeTransaction);
Transaction.addHook("afterUpdate", denormalizeTransaction);

module.exports = {
  Transaction,
  Marchand,
  Operation,
}
