const {Schema} = require("mongoose");
const conn = require("../../lib/mongo");

const operationSchema = new Schema({
  id: Number,
  price: Number,
  status: String,
  item: [String],
}, {
  _id: false,
  timestamps: false,
  versionKey: false
});

const marchandSchema = new Schema({
  id: Number,
  businessName: String,
  email: String,
  phoneNumber: String,
}, {
  _id: false,
  timestamps: false,
  versionKey: false
});

const clientSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  address: String,
}, {
  _id: false,
  timestamps: false,
  versionKey: false
});

const TransactionOperationSchema = new Schema({
  transactionId: Number,
  price: Number,
  currency: String,
  client: clientSchema,
  marchand: marchandSchema,
  operations: [operationSchema],
}, {});

const TransactionOperation = conn.model("TransactionOperation", TransactionOperationSchema);

module.exports = TransactionOperation;
