const OperationHistory = require("./OperationHistory");
const TransactionOperation = require("./TransactionOperation");

module.exports = {
  OperationHistory,
  TransactionOperation,
}
