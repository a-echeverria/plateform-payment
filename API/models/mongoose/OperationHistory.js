const {Schema} = require("mongoose");
const {OperationStatus} = require("../../lib/utils")
const conn = require("../../lib/mongo");

const OperationHistorySchema = new Schema({
  transactionId: Number,
  operationId: Number,
  status: {
    type: String,
    enum: OperationStatus,
  },
  price: Number,
  item: [String],
}, {
  timestamps: true,
});

const OperationHistory = conn.model("OperationHistory", OperationHistorySchema);

module.exports = OperationHistory;
