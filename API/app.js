require('dotenv').config()
const express = require("express");
const cors = require('cors');
const {
  TransactionRouter,
  OperationRouter,
  MarchandRouter,
  PaymentRouter,
  AccountRouter,
  OperationHistoryRouter,
  TransactionOperationRouter,
} = require("./routes");
const app = express();

app.set("view engine", "twig");
app.set("views", __dirname + "/views");
app.set("twig options", {
  allow_async: true,
  strict_variables: false
});

// Middleware
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());

// Router
app.use("/payment", PaymentRouter);
app.use("/transaction", TransactionRouter);
app.use("/operation", OperationRouter);
app.use("/marchand", MarchandRouter);
app.use("/operationhistory", OperationHistoryRouter);
app.use("/transactionoperation", TransactionOperationRouter);
app.use(AccountRouter);

app.listen(process.env.PORT || 3000, () => console.log(`server listening on ${process.env.PORT}`));
