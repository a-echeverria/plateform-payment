const atob = require("atob");
const {Marchand} = require('../models/sequelize');
const jwt = require("jsonwebtoken");

module.exports = async function AuthorizationMiddleware(req, res, next) {
  const {authorization} = req.headers;
  if (!authorization) res.sendStatus(401);
  const [type, token] = authorization.split(/\s+/);
  if (!["BASIC", "BEARER"].includes(type)) res.sendStatus(401);
  let credentials;
  if (type === "BASIC") {
    const [client_id, client_secret] = atob(token).split(':');
    credentials = {
      client_id,
      client_secret,
    }
  } else {
    const {client_id, client_secret} = jwt.verify(token, process.env.JWT_TOKEN_SECRET, (err, payload) => {
      if (err) {
        res.sendStatus(403);
        return;
      }
      return payload;
    });
    credentials = {
      client_id,
      client_secret,
    }
  }
  const marchand = await Marchand.findOne({
    where: {
      ...credentials
    }
  })
  if (!marchand) {
    res.sendStatus(403);
    return;
  }
  req.marchand = marchand.dataValues;
  next();
};