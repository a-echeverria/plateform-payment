const {Router} = require("express");
const {AuthorizationMiddleware} = require('../middlewares');
const {TransactionOperation} = require('../models/mongoose');

const router = Router();

/**
 * Methods: GET
 * Description: Get all operationHistories
 */
router.get('/',
  AuthorizationMiddleware,
  (request, response) => {
    const query = request.query;
    TransactionOperation.find(query).then(data => response.json(data));
  });

module.exports = router;
