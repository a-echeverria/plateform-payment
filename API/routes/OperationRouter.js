const {Router} = require("express");
const {Operation} = require('../models/sequelize')
const {prettifyValidationErrors} = require("../lib/utils");
const {AuthorizationMiddleware} = require('../middlewares');

const router = Router();

/**
 * Methods: GET
 * Description: Get all operations
 */
router.get('/',
  AuthorizationMiddleware,
  (request, response) => {
    Operation.findAll({
      where: request.query,
    })
      .then((data) => response.json(data))
      .catch((e) => response.sendStatus(500));
  });

/**
 * Methods: POST
 * Description: Create an Operation
 */
router.post('/',
  AuthorizationMiddleware,
  (request, response) => {
    new Operation(request.body)
      .save()
      .then((data) => response.status(201).json(data))
      .catch((e) => {
        if (e.name === "SequelizeValidationError") {
          console.error(e);
          response.status(400).json(prettifyValidationErrors(e.errors));
        } else {
          response.sendStatus(500);
        }
      });
  });

/**
 * Methods: GET
 * Description: Get an operation by id
 */
router.get('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Operation.findOne({
      where: {id},
    })
      .then((data) =>
        data === null ? response.sendStatus(404) : response.json(data)
      )
      .catch((e) => response.sendStatus(500));

  });

/**
 * Methods: PUT
 * Description: Update an operation
 */
router.put('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Operation.update(request.body, {
      where: {id},
      returning: true,
      individualHooks: true,
    })
      .then(([, [data]]) => {
          data !== undefined ? response.status(200).json(data) : response.sendStatus(404)
        }
      )
      .catch((e) => {
        if (e.name === "SequelizeValidationError") {
          response.status(400).json(prettifyValidationErrors(e.errors));
        } else {
          response.sendStatus(500);
        }
      });
  });

/**
 * Methods: DELETE
 * Description: Delete an operation
 */
router.delete('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Operation.destroy({where: {id}})
      .then((data) =>
        data === 0 ? response.sendStatus(404) : response.sendStatus(204)
      )
      .catch((e) => response.sendStatus(500));
  });

module.exports = router;
