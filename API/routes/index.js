const TransactionRouter = require("./TransactionRouter");
const OperationRouter = require("./OperationRouter");
const MarchandRouter = require("./MarchandRouter");
const PaymentRouter = require("./PaymentRouter");
const AccountRouter = require("./AccountRouter");
const OperationHistoryRouter = require('./OperationHistoryRouter');
const TransactionOperationRouter = require('./TransactionOperationRouter');

module.exports = {
  TransactionRouter,
  OperationRouter,
  MarchandRouter,
  PaymentRouter,
  AccountRouter,
  OperationHistoryRouter,
  TransactionOperationRouter,
}