const {Router} = require("express");
const {Marchand} = require('../models/sequelize')
const {prettifyValidationErrors} = require("../lib/utils");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const router = Router();

router.post("/login", async (request, response) => {
  const {username, password} = request.body;

  const marchand = await Marchand.findOne({
    where: {
      username,
    },
  }).catch(e => {
    console.error(e);
    if (e.name === "SequelizeValidationError") {
      response.status(400).json(prettifyValidationErrors(e.errors));
    } else {
      response.sendStatus(500);
    }
  });

  if (!marchand) {
    response.sendStatus(404);
    return;
  }
  const match = await bcrypt.compare(password, marchand.password);

  if (!match) {
    response.sendStatus(400);
    return;
  }

  const token = jwt.sign({
    username,
    marchandId: marchand.id,
    client_id: marchand.client_id,
    client_secret: marchand.client_secret,
    is_admin: marchand.is_admin
  }, process.env.JWT_TOKEN_SECRET);
  response.json({token}).sendStatus(200);
});

module.exports = router;
