const {Router} = require("express");
const {Operation, Marchand} = require('../models/sequelize');
const {OperationStatus, prettifyValidationErrors} = require("../lib/utils");
const http = require("http");

const router = Router();

/**
 * Methods: GET
 * Description: Get payment script
 */
router.get('/',
  async (request, response) => {
    const {operationId, marchandId} = request.query;


    if (!operationId || !marchandId) {
      response.render("errors/500.twig", {});
    }
    const operation = await Operation.findOne({
      where: {id: operationId}
    });
    if (!operation) {
      response.render("errors/404.twig", {});
      return;
    }
    const marchand = await Marchand.findOne({
      where: {id: marchandId}
    });
    if (!marchand) {
      response.render("errors/404.twig", {});
      return;
    }

    response.render('payment.twig', {
      success: encodeURI(marchand.confirmationUrl),
      cancel: encodeURI(marchand.cancelUrl),
      price: operation.price,
      currency: operation.currency,
      operationId,
    });
  });

/**
 * Methods: POST
 * Description: Success route from payment script
 */
router.post('/success', async (request, response) => {
  const {redirectUrl, operationId} = request.query;
  if (redirectUrl === undefined || operationId === undefined) {
    response.sendStatus(500);
    return;
  }
  const updatedOperation = await Operation.update({status: OperationStatus[1]}, {
    where: {id: operationId},
    returning: true,
    individualHooks: true,
  })
    .catch((e) => {
      console.error(e);
      if (e.name === "SequelizeValidationError") {
        response.status(400).json(prettifyValidationErrors(e.errors));
      } else {
        response.sendStatus(500);
      }
    });

  if (updatedOperation === undefined || updatedOperation === null) {
    response.sendStatus(404);
    return;
  }

  const url = `http://localhost:${process.env.PSP_PORT}/psp`;
  http.get(url, {}, async (res) => {
    let status;
    if (res.statusCode > 400) {
      status = OperationStatus[4];
    } else {
      status = OperationStatus[2];
    }
    const [number, operations] = await Operation.update({status}, {
      where: {id: operationId},
      returning: true,
      individualHooks: true,
    }).catch((e) => {
      console.error(e);
      if (e.name === "SequelizeValidationError") {
        response.status(400).json(prettifyValidationErrors(e.errors));
      } else {
        response.sendStatus(500);
      }
    });
    await http.request(`http://localhost:${process.env.MARCHAND_PORT}/confirm`,
      {method: 'POST', headers: {'Content-Length': Buffer.byteLength(JSON.stringify(operations[0].dataValues))}})
      .write(JSON.stringify(operations[0].dataValues));
  });

  response.writeHead(301,
    {Location: decodeURI(redirectUrl)}
  );
  response.end();
});


/**
 * Methods: GET
 * Description: Cancel route from payment script
 */

router.get('/cancel', async (request, response) => {
  const {redirectUrl, operationId} = request.query;
  if (redirectUrl === undefined || operationId === undefined) {
    response.sendStatus(500);
    return;
  }
  Operation.update({status: OperationStatus[3]}, {
    where: {id: operationId},
    returning: true,
    individualHooks: true,
  })
    .then(([, [data]]) => {
        if (data !== undefined) {
          response.writeHead(301,
            {Location: decodeURI(redirectUrl)}
          );
          response.end();
        } else
          response.sendStatus(404)
      }
    )
    .catch((e) => {
      if (e.name === "SequelizeValidationError") {
        response.status(400).json(prettifyValidationErrors(e.errors));
      } else {
        response.sendStatus(500);
      }
    });
});
module.exports = router;
