const {Router} = require("express");
const {AuthorizationMiddleware} = require('../middlewares');
const {OperationHistory} = require('../models/mongoose');

const router = Router();

/**
 * Methods: GET
 * Description: Get all operationHistories
 */
router.get('/',
  AuthorizationMiddleware,
  (request, response) => {
    const query = request.query;
    OperationHistory.find(query).then(data => response.json(data));
  });

module.exports = router;
