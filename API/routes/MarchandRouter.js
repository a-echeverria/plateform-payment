const {Router} = require("express");
const {Marchand, Transaction} = require('../models/sequelize')
const {prettifyValidationErrors, generateClientIdAndSecret} = require("../lib/utils");
const {AuthorizationMiddleware} = require('../middlewares');

const router = Router();

/**
 * Methods: GET
 * Description: Get all marchands
 */
router.get('/',
  AuthorizationMiddleware,
  (request, response) => {
    Marchand.findAll({
      where: request.query,
      include: [Transaction],
    })
      .then((data) => response.json(data))
      .catch((e) => response.sendStatus(500));
  });

/**
 * Methods: POST
 * Description: Create a Marchand
 */
router.post('/', (request, response) => {
  new Marchand(request.body)
    .save()
    .then((data) => response.status(201).json(data))
    .catch((e) => {
      console.error(e);
      if (e.name === "SequelizeValidationError") {
        response.status(400).json(prettifyValidationErrors(e.errors));
      } else {
        response.sendStatus(500);
      }
    });
});

/**
 * Methods: GET
 * Description: Get a marchand by id
 */
router.get('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Marchand.findOne({
      where: {id},
      include: [Transaction],
    })
      .then((data) =>
        data === null ? response.sendStatus(404) : response.json(data)
      )
      .catch((e) => response.sendStatus(500));

  });

/**
 * Methods: PUT
 * Description: Update a marchand
 */
router.put('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Marchand.update(request.body, {
      where: {id},
      returning: true,
      individualHooks: true,
    })
      .then(([, [data]]) => {
          data !== undefined ? response.status(200).json(data) : response.sendStatus(404)
        }
      )
      .catch((e) => {
        console.error(e);
        if (e.name === "SequelizeValidationError") {
          response.status(400).json(prettifyValidationErrors(e.errors));
        } else {
          response.sendStatus(500);
        }
      });
  });

/**
 * Methods: DELETE
 * Description: Delete a marchand
 */
router.delete('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Marchand.destroy({where: {id}})
      .then((data) =>
        data === 0 ? response.sendStatus(404) : response.sendStatus(204)
      )
      .catch((e) => response.sendStatus(500));
  });

/**
 * Methods: PUT
 * Description: Validate a Marchand & create his client_id & client_secret
 */
router.put("/:id/accept", async (request, response) => {
  const {id} = request.params;
  const {client_id, client_secret} = generateClientIdAndSecret();

  const marchand = await Marchand.update({
      confirmed: true,
      client_id,
      client_secret,
    },
    {
      where: {id},
      returning: true,
      individualHooks: true,
    }).catch((e) => {
    console.error(e);
    if (e.name === "SequelizeValidationError") {
      response.status(400).json(prettifyValidationErrors(e.errors));
    } else {
      response.sendStatus(500);
    }
  });

  if (!marchand) {
    response.sendStatus(404);
    return;
  }

  response.sendStatus(200).json(marchand);
});

module.exports = router;
