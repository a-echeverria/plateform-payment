const {Router} = require("express");
const {Transaction, Operation} = require('../models/sequelize')
const {prettifyValidationErrors, OperationStatus} = require("../lib/utils");
const {AuthorizationMiddleware} = require('../middlewares');

const router = Router();

/**
 * Methods: GET
 * Description: Get all transactions
 */
router.get('/',
  AuthorizationMiddleware,
  (request, response) => {
    Transaction.findAll({
      where: request.query,
      include: [Operation],
    })
      .then((data) => response.json(data))
      .catch((e) => response.sendStatus(500));
  });

/**
 * Methods: POST
 * Description: Create a transaction
 */
router.post('/',
  AuthorizationMiddleware,
  (request, response) => {
    const {clientFirstName, clientLastName, clientAddress, clientEmail, item, price, currency} = request.body;
    new Transaction({clientFirstName, clientLastName, clientAddress, clientEmail, marchandId: request.marchand.id})
      .save()
      .then((data) => {
        new Operation({
          item,
          price,
          transactionId: data.id,
          status: OperationStatus[0],
          currency,
        })
          .save()
          .then(() => response.status(201).json(data));
      })
      .catch((e) => {
        if (e.name === "SequelizeValidationError") {
          console.error(e);
          response.status(400).json(prettifyValidationErrors(e.errors));
        } else {
          response.sendStatus(500);
        }
      });
  });

/**
 * Methods: GET
 * Description: Get a transaction by id
 */
router.get('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Transaction.findOne({
      where: {id},
      include: [Operation]
    })
      .then((data) =>
        data === null ? response.sendStatus(404) : response.json(data)
      )
      .catch((e) => response.sendStatus(500));

  });

/**
 * Methods: PUT
 * Description: Update a transaction
 */
router.put('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Transaction.update(request.body, {
      where: {id},
      returning: true,
      individualHooks: true,
    })
      .then(([, [data]]) =>
        data !== undefined ? response.status(200).json(data) : response.sendStatus(404)
      )
      .catch((e) => {
        if (e.name === "SequelizeValidationError") {
          response.status(400).json(prettifyValidationErrors(e.errors));
        } else {
          response.sendStatus(500);
        }
      });
  });

/**
 * Methods: DELETE
 * Description: Delete a transaction
 */
router.delete('/:id',
  AuthorizationMiddleware,
  (request, response) => {
    const {id} = request.params;
    Transaction.destroy({where: {id}})
      .then((data) =>
        data === 0 ? response.sendStatus(404) : response.sendStatus(204)
      )
      .catch((e) => response.sendStatus(500));
  });

module.exports = router;
