require('dotenv').config()
const express = require("express");
const PspRouter = require("./routes/PspRouter");

const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded());

app.use("/psp", PspRouter);

app.listen(process.env.PORT || 3001, () => console.log(`PSP listening on ${process.env.PORT}`));
