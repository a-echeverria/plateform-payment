import "./App.css";
import {Credentials, Header, Page, ShowItem} from "./components";
import {BrowserRouter, Route} from "react-router-dom";
import {CredentialsProvider, ListProvider} from "./contexts";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CredentialsProvider>
          <BrowserRouter>
            <Route path="/admin" exact>
              <Header/>
              <Credentials/>
            </Route>
            <ListProvider>
              <Route path="/" exact>
                <Page/>
              </Route>
              <Route path="/items/:id" exact>
                <Header/>
                <ShowItem/>
              </Route>
            </ListProvider>
          </BrowserRouter>
        </CredentialsProvider>
      </header>
    </div>
  );
}

export default App;