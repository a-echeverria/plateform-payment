import {createContext, useState} from "react";

export const ClientInfoContext = createContext();
export function ClientInfoProvider({children}) {
    const [client, setClient] = useState({
        firstname: "",
        lastname: "",
        email: "",
        address: "",
    });

  return (
    <ClientInfoContext.Provider value={{client, setClient}}>
        {children}
    </ClientInfoContext.Provider>
  );
}
