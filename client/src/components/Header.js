import React from "react";
import { Link } from "react-router-dom";

export function Header() {
  return (
    <nav>
      Website
      <Link to="/">Home</Link>
      <Link to="/admin">Admin</Link>
    </nav>
  );
}
