import React from "react";
import {Body} from "./Body";
import {Header} from "./Header";

export function Page() {
  return (
    <div>
      <Header />
      <Body />
    </div>
  );
}
