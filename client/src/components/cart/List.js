import React, {useContext, useState} from "react";
import {ListContext} from "../../contexts";
import {CreateTransactionButton} from "./CreateTransactionButton";
import {ListItem} from "./ListItem";
import {AddEditItem} from "./AddEditItem";
import {ClientInfoForm} from "./ClientInfoForm";

export function List() {
  const {list, totalPrice, currencyEnum} = useContext(ListContext);
  const [selectedItem, setSelectedItem] = useState();
  const [currency, setCurrency] = useState("DOLLARS");

  const handleEditItem = (item) => setSelectedItem(item);

  return (
    <div>
      <ClientInfoForm/>
      <select value={currency} name="currency" onChange={(event) => {setCurrency(event.target.value)}}>
        {currencyEnum.map(c => (
          <option key={c}>{c}</option>
        ))}
      </select><br/>
      <AddEditItem selectedItem={selectedItem}/>
      <ul>
        {list.map((item) => (
          <ListItem key={item.id} item={item} onEdit={handleEditItem}/>
        ))}
      </ul>
      <p>Total price: {totalPrice}</p>
      <CreateTransactionButton currency={currency}/>
    </div>
  );
}
