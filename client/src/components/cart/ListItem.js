import { useContext, useMemo } from "react";
import { ListContext } from "../../contexts";
import {Button} from "../lib";
import { Link } from "react-router-dom";

export function ListItem({ item, onEdit }) {
  const { deleteItem } = useContext(ListContext);

  return useMemo(
    () => (
      <li>
        <Link to={`/items/${item.id}`}>{item.name}</Link> {item.unitPrice}{" "}
        {item.quantity}
        <Button title="delete" onClick={() => deleteItem(item)} />
        <Button title="edit" onClick={() => onEdit(item)} />
      </li>
    ),
    [item]
  );
}
