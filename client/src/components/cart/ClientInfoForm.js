import React, { useContext } from "react";
import { ClientInfoContext } from "../../contexts";

export function ClientInfoForm() {

    const {setClient, client} = useContext(ClientInfoContext);

    const handleChange = (event) => {
        setClient({
          ...client,
          [event.target.name]: event.target.value,
        });
      };

  return (
      <>
    <div>
      prenom
      <input
        value={client.firstname}
        type="text"
        name="firstname"
        onChange={handleChange}
      />
      <br/>
      nom
      <input
        value={client.lastname}
        type="text"
        name="lastname"
        onChange={handleChange}
      />
      <br/>
      email
      <input
        value={client.email}
        type="text"
        name="email"
        onChange={handleChange}
      />
      <br/>
      adresse
      <input
        value={client.address}
        type="text"
        name="address"
        onChange={handleChange}
      />
    </div>
    </>
  );
}
