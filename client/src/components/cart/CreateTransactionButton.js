import {useContext} from "react";
import {CredentialsContext, ListContext, ClientInfoContext} from "../../contexts";
import {Button} from "../lib";

export function CreateTransactionButton({currency}) {
  const {token} = useContext(CredentialsContext);
  const {price, list} = useContext(ListContext);
  const {client} = useContext(ClientInfoContext);
  const options = {
    mode: "cors",
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": '*',
      "authorization": "BASIC " + token,
    },
  };
  const item = [];
  list.forEach(element => {
    for(let i = 0; i < element.quantity; i ++)
    {
      item.push(element.name)
    }
  });
  const generateTransaction = async () => {
    const transaction = await fetch("http://localhost:3000/transaction", {
      ...options,
      method: "POST",
      body: JSON.stringify({
        clientFirstName: client.firstname,
        clientLastName: client.lastname,
        clientEmail: client.email,
        clientAddress: client.address,
        price,
        item,
        currency,
      }),
    }).then(res => res.json());

    const operation = await fetch(`http://localhost:3000/operation?transactionId=${transaction.id}`, {
      ...options
    }).then(res => res.json());

   window.location.href = `http://localhost:3000/payment?operationId=${operation[0].id}&marchandId=${transaction.marchandId}`;
  };

  return (
    <Button title="create transaction" onClick={() => generateTransaction()}/>
  );
}
