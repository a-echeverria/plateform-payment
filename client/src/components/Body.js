import React from "react";
import {List} from "./cart";
import {ClientInfoProvider} from "../contexts"

export function Body() {
  return (
    <div>
      <ClientInfoProvider>
        <List />
      </ClientInfoProvider>
    </div>
  );
}
