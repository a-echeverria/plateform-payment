require('dotenv').config()
const express = require("express");
const ConfirmOperationRouter = require("./routes/ConfirmOperationRouter");

const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded());

app.use("/confirm", ConfirmOperationRouter);

app.listen(process.env.PORT || 3001, () => console.log(`Marchand listening on ${process.env.PORT}`));
