import {createContext, useEffect, useState,} from "react";

export const ListContext = createContext();

export function ListProvider({children}) {
  const [list, setList] = useState([]);
  const token = localStorage.getItem('jwt_token');
  const options = {
    mode: "cors",
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": '*',
      "authorization": "BEARER " + token,
    }
  };

  const defaultList = async () => {
    return await fetch(`http://localhost:3000/marchand`, {
      ...options,
    }).then(res => res.json());
  };

  useEffect(async () => setList(await defaultList()), []);

  const deleteMarchand = (marchand) => {
    setList(list.filter(m => m !== marchand));
    fetch(`http://localhost:3000/marchand/${marchand.id}`, {
      ...options,
      method: "DELETE",
    }).then(res => res.json());
  };

  const getMarchand = async ({id}) => {
    return await fetch(`http://localhost:3000/marchand/${id}`, {
      ...options
    }).then(res => res.json());
  };

  const validate = (marchand) => {
    return fetch(`http://localhost:3000/marchand/${marchand.id}/accept`, {
      ...options,
      method: "PUT"
    }).then(res => res.json());
  }

  const updateMarchand = (id, marchand) => {
    return fetch(`http://localhost:3000/marchand/${id}`, {
      ...options,
      method: "PUT",
      body: JSON.stringify(marchand)
    }).then(res => res.json());
  };

  return (
    <ListContext.Provider value={{list, deleteMarchand, getMarchand, validate, updateMarchand}}>
      {children}
    </ListContext.Provider>
  );
}
