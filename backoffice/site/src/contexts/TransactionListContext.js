import {createContext, useEffect, useState,} from "react";
import jwtDecode from "jwt-decode";

export const TransactionListContext = createContext();

export function TransactionListProvider({children, marchand}) {
  const [list, setList] = useState([]);
  const token = localStorage.getItem('jwt_token');
  const profil = jwtDecode(token);
  const options = {
    mode: "cors",
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": '*',
      "authorization": "BEARER " + token,
    }
  };

  const sortColumns = ["id", "marchandId", "clientFirstName", "clientLastName", "clientEmail", "clientAddress", "currency", "status"];

  const sortList = async ({column, elem}) => {
    if (column === "id" || column === "marchandId") elem = parseInt(elem) || elem;
    if (elem === '') {
      setList(await defaultList());
    } else if (column === "status" || column === "currency") {
      setList(list.filter(l => {
        let found = false;
        for (let i = 0; i < l.Operations.length; i++) {
          if ((l.Operations[i])[column] === elem) {
            found = true;
          }
        }
        return found;
      }));
    } else {
      setList(list.filter(l => {
        return l[column] === elem;
      }));
    }
  };

  const resetList = async () => {
    setList([]);
    setList(await defaultList());
  };

  const defaultList = async () => {
    const url = marchand ? `http://localhost:3000/transaction?marchandId=${profil.marchandId}` : `http://localhost:3000/transaction`;
    return await fetch(url, {
      ...options,
    }).then(res => res.json());
  };

  useEffect(async () => setList(await defaultList()), []);

  const deleteTransaction = (transaction) => {
    setList(list.filter(m => m !== transaction));
    fetch(`http://localhost:3000/transaction/${transaction.id}`, {
      ...options,
      method: "DELETE",
    }).then(res => res.json());
  };

  const getTransaction = async ({id}) => {
    return await fetch(`http://localhost:3000/transaction/${id}`, {
      ...options
    }).then(res => res.json());
  };

  const refundOperation = (operation) => {
    return fetch(`http://localhost:3000/operation/`, {
      ...options,
      method: "POST",
      body: JSON.stringify({
        "status": "REFUND",
        "transactionId": operation.transactionId,
        "currency": operation.currency,
        "item": operation.item,
        "price": (operation.price * -1),
      }),
    }).then(res => res.json());
  };


  return (
    <TransactionListContext.Provider
      value={{list, deleteTransaction, getTransaction, refundOperation, sortColumns, sortList, resetList}}>
      {children}
    </TransactionListContext.Provider>
  );
}
