import React, {createContext, useEffect, useState} from 'react';
import jwtDecode from 'jwt-decode';

export const CredentialsContext = createContext();

export function CredentialsProvider({children}) {
  const [token, setToken] = useState();
  const [profil, setProfil] = useState();

  useEffect(() => {
    const token = localStorage.getItem('jwt_token');
    setToken(token);
    setProfil(token && jwtDecode(token));
  }, []);

  const login = function ({username, password}) {
    return fetch('http://localhost:3000/login', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({username, password}),
    })
      .then(res => res.json())
      .then(data => {
        localStorage.setItem('jwt_token', data.token);
        setToken(data.token);
        setProfil(jwtDecode(data.token));
      });
  };

  const logout = function () {
    localStorage.removeItem('jwt_token');
    setToken(null);
    setProfil(null);
  };

  const register = function (user) {
    return fetch('http://localhost:3000/marchand', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(res => res.json());
  }

  return (
    <CredentialsContext.Provider value={{token, profil, login, logout, register}}>
      {children}
    </CredentialsContext.Provider>
  );
}
