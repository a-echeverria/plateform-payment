import {createContext, useEffect, useState,} from "react";
import jwtDecode from "jwt-decode";

export const TransactionOperationContext = createContext();

export function TransactionOperationProvider({children, marchand}) {
  const [transactionOperationList, setList] = useState([]);
  const token = localStorage.getItem('jwt_token');
  const profil = jwtDecode(token);
  const options = {
    mode: "cors",
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": '*',
      "authorization": "BEARER " + token,
    }
  };

  const defaultList = async () => {
    const url = marchand ? `http://localhost:3000/transactionoperation?marchand.id=${profil.marchandId}` : `http://localhost:3000/transactionoperation`;
    return await fetch(url, {
      ...options,
    }).then(res => res.json());
  };

  useEffect(async () => setList(await defaultList()), []);

  const OperationHistory = (transactionId) => {
    const url = `http://localhost:3000/operationhistory?transactionId=${transactionId}`;
    return fetch(url, {
      ...options,
    }).then(res => res.json());
  };

  return (
    <TransactionOperationContext.Provider value={{transactionOperationList, OperationHistory}}>
      {children}
    </TransactionOperationContext.Provider>
  );
}
