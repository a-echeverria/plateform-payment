import {LoginForm} from "./LoginForm";
import { CredentialsContext } from "../../contexts";
import { useContext } from "react";

export function Login() {
  const { login } = useContext(CredentialsContext);
  return (
    <>
        <LoginForm
          login={login}
        />
    </>
  );
}
