import React, { useState } from "react";
import {Button} from "../lib/";

export function LoginForm({ login }) {
  const [user, setUser] = useState({
      username: "",
      password: "",
    }
  );

  const handleChange = (event) => {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
     login(user);
    };

  return (
    <div>
      Username
      <input value={user.username} name="username" onChange={handleChange} /><br/>
      Password
      <input
        value={user.password}
        type="password"
        name="password"
        onChange={handleChange}
      /><br/>
      <Button title="Submit" onClick={handleSubmit} />
    </div>
  );
}
