import { CredentialsContext } from "../../contexts";
import { useContext, useState } from "react";
import { Button } from "../lib"

export function Register() {
  const { register } = useContext(CredentialsContext);
  const [user, setUser] = useState({
    "businessName": "",
    "username": "",
    "email": "",
    "phoneNumber": "",
    "currency": "",
    "confirmationUrl": "",
    "cancelUrl": "",
    "password": "",
    "confirmed": false,
    "client_id": null,
    "client_secret": null
  }
);

const handleChange = (event) => {
  setUser({
    ...user,
    [event.target.name]: event.target.value,
  });
};

const handleSubmit = () => {
  register(user);
  window.location.href = "/";
};

return (
  <div>
    Username
    <input value={user.username} name="username" onChange={handleChange} /><br/>
    Password
    <input value={user.password} type="password" name="password" onChange={handleChange}/><br/>
    email
    <input value={user.email} name="email" onChange={handleChange} /><br/>
    phoneNumber
    <input value={user.phoneNumber} name="phoneNumber" onChange={handleChange} /><br/>
    currency
    <input value={user.currency} name="currency" onChange={handleChange} /><br/>
    businessName
    <input value={user.businessName} name="businessName" onChange={handleChange} /><br/>
    URL de succes
    <input value={user.confirmationUrl} name="confirmationUrl" onChange={handleChange} /><br/>
    URL d'erreur
    <input value={user.cancelUrl} name="cancelUrl" onChange={handleChange} /><br/>
    <Button title="Submit" onClick={handleSubmit} />
  </div>
);
}