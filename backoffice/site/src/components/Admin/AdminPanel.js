import {useContext} from "react";
import {TransactionOperationContext} from "../../contexts";

export function AdminPanel({}) {
  const {transactionOperationList} = useContext(TransactionOperationContext);

  const average = () => {
    let sum = 0;
    let length = 0;
    transactionOperationList.map(transaction => {
      sum += transaction?.price || 0;
      const isRefund = transaction.operations.find((operation) => operation.status === "REFUND");
      if (!isRefund) length++;
    })
    return length === 0 ? 0 : sum / length;
  };

  const refundCount = () => {
    let sum = 0;
    transactionOperationList.map(transaction => {
      transaction.operations.map(operation => {
        if (operation.status === "REFUND") sum++;
      })
    })
    return sum;
  };

  const mostUsedCurrency = () => {
    const euro = "EURO";
    let euroCount = 0;
    const dollars = "DOLLARS";
    let dollarsCount = 0;
    const yen = "YEN";
    let yenCount = 0;
    let others = 0;

    transactionOperationList.forEach(transaction => {
      if (transaction.currency === euro) euroCount++;
      else if (transaction.currency === dollars) dollarsCount++;
      else if (transaction.currency === yen) yenCount++;
      else others++;
    });
    if (euroCount > dollarsCount && euroCount > yenCount && euroCount > others) return euro;
    else if (dollarsCount > euroCount && dollarsCount > yenCount && dollarsCount > others) return dollars;
    else if (yenCount > dollarsCount && yenCount > euroCount && yenCount > others) return yen;
    else return '';
  };

  return (
    <div>
      Devise la plus utilisé: {mostUsedCurrency()} <br/>
      Moyenne des paniers: {average()} <br/>
      Nombre total de transaction : {transactionOperationList.length} <br/>
      Nombre d'opération remboursé: {refundCount()} <br/>
    </div>
  );
}
