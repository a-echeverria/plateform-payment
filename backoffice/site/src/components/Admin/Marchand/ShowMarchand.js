import {useContext} from "react";
import {useParams} from "react-router";
import {useAsync} from "react-async"
import {CredentialsContext, ListContext} from "../../../contexts";
import {Button} from "../../lib";
import {Link} from "react-router-dom";

export function ShowMarchand() {
  const {id} = useParams();
  const {getMarchand, validate} = useContext(ListContext);
  const {logout} = useContext(CredentialsContext);

  const {data, error} = useAsync({promiseFn: getMarchand, id});

  const handleReset = () => {
    validate(data);
    logout();
  }

  return (
    <>
      {data && (
        <div>
          Nom du commerce : {data.businessName}<br/>
          cancelUrl: {data.cancelUrl}<br/>
          client_id: {data.client_id}<br/>
          client_secret: {data.client_secret}<br/>
          confirmationUrl: {data.confirmationUrl}<br/>
          createdAt: {data.createdAt}<br/>
          currency: {data.currency}<br/>
          email: {data.email}<br/>
          phoneNumber: {data.phoneNumber}<br/>
          username: {data.username}<br/>
          <br/><br/>
          Nombre de transaction : {data.Transactions.length}
          <br/><br/>
          <Button title={"Reset credentials"} onClick={handleReset}/><br/>
          <Link to="/transaction">Afficher mes transactions</Link><br/>
          <Link to={`/admin/marchand/edit/${data.id}`}>Éditer</Link>
        </div>
      )}
    </>
  );
}