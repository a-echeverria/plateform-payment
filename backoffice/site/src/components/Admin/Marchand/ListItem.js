import {useContext, useMemo} from "react";
import {ListContext} from "../../../contexts";
import {Button} from "../../lib";
import {Link} from "react-router-dom";

export function ListItem({marchand}) {
  const {deleteMarchand, validate} = useContext(ListContext);

  return useMemo(
    () => (
      <li>
        <Link to={`/admin/marchand/${marchand.id}`}>{marchand.username}</Link>
        {!marchand.confirmed && (
          <Button title="Valider" onClick={() => validate(marchand)}/>
        )}
        <Button title="Supprimer" onClick={() => deleteMarchand(marchand)}/>
      </li>
    ),
    [marchand]
  );
}
