import React, {useContext} from "react";
import {ListContext} from "../../../contexts";
import {ListItem} from "./ListItem";

export function List() {
  const {list} = useContext(ListContext);

  return (
    <div>
      <ul>
        {list.length > 0 && (
          list.map(marchand => (
          <ListItem key={marchand.id} marchand={marchand}/>
        )))}
        {list.length === 0 && (
          <div>
            Il n'y a aucun marchand en attente !
          </div>
        )}
      </ul>
    </div>
  );
}
