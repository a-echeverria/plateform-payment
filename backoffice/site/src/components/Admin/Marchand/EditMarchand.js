import {useContext, useEffect, useState} from "react";
import {useParams} from "react-router";
import {ListContext} from "../../../contexts";
import {Button} from "../../lib";

export function EditMarchand({}) {
  const {id} = useParams();
  const {updateMarchand, getMarchand} = useContext(ListContext);
  const [update, setUpdate] = useState()


  useEffect(async () => {
    const marchand = await getMarchand({id});
    setUpdate({
      businessName: marchand.businessName,
      cancelUrl: marchand.cancelUrl,
      confirmationUrl: marchand.confirmationUrl,
      currency: marchand.currency,
      email: marchand.email,
      phoneNumber: marchand.phoneNumber,
      username: marchand.username,
    })
  }, []);

  const handleSubmit = () => {
    updateMarchand(id, update);
  }

  const handleChange = (event) => {
    setUpdate({
      ...update,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <>
      {update && (
        <div>
          Nom du commerce: <br/>
          <input value={update.businessName} name="businessName" onChange={handleChange}/><br/>
          cancelUrl: <br/>
          <input value={update.cancelUrl} name="cancelUrl" onChange={handleChange}/><br/>
          confirmationUrl: <br/>
          <input value={update.confirmationUrl} name="confirmationUrl" onChange={handleChange}/><br/>
          currency: <br/>
          <input value={update.currency} name="currency" onChange={handleChange}/><br/>
          email: <br/>
          <input value={update.email} name="email" onChange={handleChange}/><br/>
          phoneNumber: <br/>
          <input value={update.phoneNumber} name="phoneNumber" onChange={handleChange}/><br/>
          username: <br/>
          <input value={update.username} name="username" onChange={handleChange}/><br/>
          <Button title={"Mettre à jour"} onClick={handleSubmit}/><br/>
        </div>
      )}
    </>
  );
}