import {useContext, useMemo} from "react";
import {TransactionListContext} from "../../../contexts";
import {Button} from "../../lib";

export function OperationListItem({operation}) {
  const {refundOperation} = useContext(TransactionListContext);

  const displayItems = (items) => {
    let item = '';
    if (!items) return item
    items.map(it => {
      item+= ' ' + it;
    });
    return item;
  };

  return useMemo(
    () => (
      <li>
        {operation.status === "VALIDATED" && (
          <div>
            <Button title="Rembourser" onClick={() => refundOperation(operation)}/>
            <br/>
          </div>
        )}
        Prix: {operation.price} {operation.currency}<br/>
        Status: {operation.status}<br/>
        Items: {displayItems(operation.item)}<br/>
      </li>
    ),
    [operation]
  );
}
