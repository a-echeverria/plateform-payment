import {useContext} from "react";
import {useParams} from "react-router";
import {useAsync} from "react-async"
import {TransactionListContext} from "../../../contexts";
import {OperationListItem} from "./OperationListItem";
import {Link} from "react-router-dom";

export function ShowTransaction({}) {
  const {id} = useParams();
  const {getTransaction} = useContext(TransactionListContext);

  const {data, error} = useAsync({promiseFn: getTransaction, id});


  return (
    <>
      {data && (
        <div>
          Transaction:<br/><br/>
          Prénom du client: {data.clientFirstName},<br/>
          Nom du client: {data.clientLastName},<br/>
          Adresse du client: {data.clientAddress},<br/>
          Email du client: {data.clientEmail},<br/>
          Marchand associé à la transaction: {data.marchandId},<br/><br/><br/>

          Operation: <br/><br/>
          {data.Operations.map(operation => (
            <OperationListItem key={operation.id} operation={operation}/>
          ))} <br/><br/>

          <Link to={`/history/operation/${data.id}`}>Historique des opérations</Link>
        </div>
      )}
    </>
  );
}