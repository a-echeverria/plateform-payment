import React, {useContext, useState} from "react";
import {TransactionListContext} from "../../../contexts";
import {TransactionListItem} from "./TransactionListItem";
import {AdminPanel} from "../AdminPanel";
import {Button} from "../../lib";

export function TransactionList({marchand}) {
  const {list, sortColumns, sortList, resetList} = useContext(TransactionListContext);
  const [search, setSearch] = useState({
    column: '',
    elem: '',
  });

  const handleChange = (event) => {
    setSearch({
      ...search,
      [event.target.name]: event.target.value,
    });
  };

  const handleSearch = () => {
    sortList(search);
  };

  const handleReset = () => {
    resetList();
  };

  return (
    <div>
      {marchand && (
        <div>
          Panel:<br/><br/>
          <AdminPanel/>
          <br/><br/><br/>
        </div>
      )}

      <select value={search.column} name="column" onChange={handleChange}>
        {sortColumns.map(column => (
          <option key={column}>{column}</option>
        ))}
      </select>
      <input name="elem" value={search.elem} onChange={handleChange}/>
      <Button title="Rechercher" onClick={handleSearch}/>
      <Button title="Reset recherche" onClick={handleReset}/>
      <br/>

      <ul>
        {list.length > 0 && (
          list.map(transaction => (
          <TransactionListItem key={transaction.id} transaction={transaction} marchand={marchand}/>
        )))}
        {list.length === 0 && (
          <div>
            Il n'y a aucun transaction !
          </div>
        )}
      </ul>
    </div>
  );
}
