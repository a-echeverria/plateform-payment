import {useContext, useMemo} from "react";
import {TransactionListContext} from "../../../contexts";
import {Button} from "../../lib";
import {Link} from "react-router-dom";

export function TransactionListItem({transaction, marchand}) {
  const {deleteTransaction, validate} = useContext(TransactionListContext);

  const totalFromTransaction = (transaction) => {
    let sum = 0;
    transaction.Operations.map(operation => {
      sum += operation.price;
    })
    return sum;
  };

  const path = () => {
    const url = `/transaction/${transaction.id}`;
    return marchand ? url : '/admin' + url;
  }

  return useMemo(
    () => (
      <li>
        <Link to={path}>{transaction.id}, total : {totalFromTransaction(transaction)}</Link>
        <Button title="Supprimer" onClick={() => deleteTransaction(transaction)}/>
      </li>
    ),
    [transaction]
  );
}
