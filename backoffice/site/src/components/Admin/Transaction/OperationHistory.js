import {useContext, useEffect, useState} from "react";
import {TransactionOperationContext} from "../../../contexts";
import {useParams} from "react-router";

export function OperationHistory() {
  const {id} = useParams();
  const {OperationHistory} = useContext(TransactionOperationContext);
  const [operations, setOperations] = useState([]) ;

  useEffect(async () => setOperations(await OperationHistory(id)), []);

  const displayItems = (items) => {
    let item = '';
    if (!items) return item
    items.map(it => {
      item+= it + ', ';
    });
    return item.slice(0, -2);
  };

  return (
    <div>
      {operations && (
        <ul>
          {operations.map(operation => (
            <li key={operation._id}>
              status: {operation.status}<br/>
              price: {operation.price}<br/>
              items: {displayItems(operation.item)}<br/>
              date: {operation.createdAt}<br/><br/>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
