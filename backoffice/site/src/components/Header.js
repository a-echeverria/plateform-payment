import React, {useContext} from "react";
import { Link } from "react-router-dom";
import {CredentialsContext} from "../contexts";

export function Header() {
  const {token, profil, logout} = useContext(CredentialsContext);

  return (
    <nav>
      <Link to="/">Home</Link>

      {token && profil && profil.is_admin && ( 
      <Link to="/admin">Admin</Link>
      )}
      {!token && (
        <Link to="/login">Login</Link>
      )}
      {!token && (
        <Link to="/register">S'inscrire</Link>
      )}
      {token && profil && (
        <Link to={`/admin/marchand/${profil.marchandId}`}>Mon compte</Link>
      )}
      {token && (
        <a onClick={logout}>Se déconnecter</a>
      )}
    </nav>
  );
}
