import './App.css';
import {CredentialsProvider, ListProvider, TransactionListProvider, TransactionOperationProvider} from "./contexts";
import {BrowserRouter, Link, Route} from "react-router-dom";
import {
  AdminPanel,
  Header,
  List,
  Login,
  OperationHistory,
  Page,
  Register,
  ShowMarchand,
  ShowTransaction,
  TransactionList,
  EditMarchand,
} from "./components";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CredentialsProvider>
          <BrowserRouter>
            <Route path="/admin" exact>
              <Header/>
              <Link to={"/admin/marchand"}>Liste de marchands</Link>
              <Link to={"/admin/transaction"}>Liste des transactions</Link>
              <TransactionOperationProvider>
                <AdminPanel/>
              </TransactionOperationProvider>
            </Route>
            <Route path="/admin/marchand" exact>
              <Header/>
              <ListProvider>
                <List/>
              </ListProvider>
            </Route>
            <Route path="/admin/marchand/:id" exact>
              <Header/>
              <ListProvider>
                <ShowMarchand/>
              </ListProvider>
            </Route>
            <Route path="/admin/transaction" exact>
              <Header/>
              <TransactionListProvider>
                <TransactionList/>
              </TransactionListProvider>
            </Route>
            <Route path="/admin/transaction/:id" exact>
              <Header/>
              <TransactionListProvider>
                <ShowTransaction/>
              </TransactionListProvider>
            </Route>
            <Route path="/admin/marchand/edit/:id" exact>
              <Header/>
              <ListProvider>
                <EditMarchand/>
              </ListProvider>
            </Route>
            <Route path="/transaction/:id" exact>
              <Header/>
              <TransactionListProvider>
                <ShowTransaction/>
              </TransactionListProvider>
            </Route>
            <Route path="/transaction" exact>
              <Header/>
              <TransactionListProvider marchand={true}>
                <TransactionOperationProvider marchand={true}>
                  <TransactionList marchand={true}/>
                </TransactionOperationProvider>
              </TransactionListProvider>
            </Route>
            <Route path="/history/operation/:id" exact>
              <Header/>
              <TransactionOperationProvider>
                <OperationHistory/>
              </TransactionOperationProvider>
            </Route>
            <Route path="/" exact>
              <Header/>
              <Page/>
            </Route>
            <Route path="/login" exact>
              <Header/>
              <Login/>
            </Route>
            <Route path="/register" exact>
              <Header/>
              <Register/>
            </Route>
          </BrowserRouter>
        </CredentialsProvider>
      </header>
    </div>
  );
}

export default App;
